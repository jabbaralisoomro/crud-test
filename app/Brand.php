<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //

    public function mobileStudent(){
        return $this->hasOneThrough(Student::class, Mobile::class);
    }
}
