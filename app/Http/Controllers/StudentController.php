<?php
#authur Jabbar

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests\studentRequest;
use App\Jobs\WelcomeJob;
use App\Mail\WelcomeMail;
use App\Student;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class StudentController extends Controller
{

    private $student;

    public function __construct()
    {

        $this->student = new Student();

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //

//            many to many
//        dd(Student::with('teachers')->get());

        // through one
//        dd(Brand::with('mobileStudent')->get());
        $students = $this->student->getAllStudent();


        return view("student.index", compact("students"));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //

        return view("student.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(studentRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('first_name', 'last_name', 'gender', 'email');
            $user=$this->student->createStudent($data);
            WelcomeJob::dispatch($user)->delay(now()->addMinutes(1));
        } catch (Exception $e) {
            DB::rollBack();
            Log::debug($e);
        } finally {
            DB::commit();
        }


//        dd("working");

        return redirect()->route('students.index')->with(["sucess" => __("error.record.created")]);
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $student = $this->student->getStudent($id);
        return view("student.edit", compact('student'));
    }

    /**
     * @Authur :Jabbar Ali Soomro
     * @Dated
     */

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(studentRequest $request, $id)
    {
        //
        DB::beginTransaction();

        try {
            $data = $request->only('first_name', 'last_name', 'gender', 'email');
            $this->student->updateStudent($data, $id);
        } catch (Exception $e) {
            DB::rollBack();
            Log::debug($e);
        } finally {
            DB::commit();
        }


        return redirect()->route('students.index')->with(["sucess" => __("error.record.updated")]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {

            $this->student->deleteStudent($id);
        } catch (Exception $e) {
            DB::rollBack();
            Log::debug($e);
        } finally {
            DB::commit();
        }

        return redirect()->route('students.index')->with(["sucess" => __("error.record.deleted")]);
        //
    }
}
