<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class studentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'first_name' => 'required',
            'last_name' => 'required',
            "email" => 'required|email|unique:students,email,' . $this->id,
            "gender" => "required|in:" . implode(',', config('contact.gender')),
            //
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'email is already already used in our systems',

        ];

    }
}
