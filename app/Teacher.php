<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    //

    public function students(){
        return $this->belongsToMany(Student::class,'student_teacher','teacher_id','student_id');
    }


}
