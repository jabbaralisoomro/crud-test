<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;
use mysql_xdevapi\Exception;

class insertHours extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:hours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'insert post in every hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            factory(Post::class, 3)->create()->each(function ($post, $index) {
                $post->type ='hourly';
                $post->save();
            });
        }
        catch (Exception $exception){
            echo $exception ."<br>";
        }

    }
}
