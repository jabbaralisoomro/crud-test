<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;
    //
    protected $fillable=['first_name','last_name',"email","gender"];

   function getAllStudent(){

       return Student::all();

   }
    function createStudent($data){

        return Student::create($data);

    }
    function updateStudent($data,$id){

        return Student::where('id',$id)->update($data);

    }
    function deleteStudent($id){

        return Student::find($id)->delete();

    }
    function getStudent($id){

        return Student::find($id);

    }
    function teachers(){
        return $this->belongsToMany(Teacher::class,'student_teacher','student_id','teacher_id');

    }
}
