<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = 'jabbar.ali@nextgeni.net';
        $subject = 'This is a demo!';
        $name = 'Jane Doe';

        return $this->view('email.test')
            ->subject($subject)
            ->with([ 'test_message' => $this->data['message'] ]);
    }
}
