<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    <h2>Student <a href="{{route("students.create")}}" style="float:right"  >create</a></h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($students as $student)
            <tr>
                <td>{{$student->first_name}}</td>
                <td>{{$student->last_name}}</td>
                <td>{{$student->email}}</td>
                <td>{{$student->gender}}</td>
                <th>
                    <form action="{{route('students.destroy',$student->id)}}"  onsubmit="return window.confirm('are you sure you want to delete')" method="post">
                        @method("delete")
                       {{csrf_field()}}
                        <input type="submit" value="delete">
                    </form>
                    <a href="{{route('students.edit',$student->id)}}">Edit</a>
                </th>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if(\Session::get('sucess'))
    <div>{{\Session::get('sucess')}}</div>
    @endif
</div>

</body>
</html>

