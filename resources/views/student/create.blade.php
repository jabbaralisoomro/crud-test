<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    <h1>Create Student</h1>
    <form action="{{route("students.store")}}" method="post">
        {{csrf_field()}}

            <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Last Name">

                @if($errors->has('first_name'))
                    <small id="emailHelp" class="form-text text-muted">{{ $errors->first('first_name') }}</small>

                @endif

        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text"  name="last_name" class="form-control" id="exampleInputEmail1"  value="{{old('last_name')}}" aria-describedby="emailHelp" placeholder="Enter Last Name">

            @if($errors->has('last_name'))
                <small id="emailHelp" class="form-text text-muted">{{ $errors->first('last_name') }}</small>

            @endif

        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" name="email" class="form-control"  value="{{old('email')}}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email Name">
            @if($errors->has('email'))
                <small id="emailHelp" class="form-text text-muted">{{ $errors->first('email') }}</small>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Gender</label>

            <select name="gender" class="form-control" id="exampleFormControlSelect1">
                @foreach(config('contact.gender') as $gender )
                <option  @if($gender==old('gender')) selected  @endif  value="{{$gender}}">{{$gender}}</option>
                @endforeach

            </select>
            @if($errors->has('gender'))
                <small id="emailHelp" class="form-text text-muted">{{ $errors->first('gender') }}</small>

            @endif

        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>

