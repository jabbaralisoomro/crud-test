<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('sent-email',"EmailController@sentEmail");

Route::resource("students","StudentController");


Route::get('/debug-sentry', function () {
    throw new Exception('hello error!');
});

Route::get('/', function () {
    return view('welcome');
});

